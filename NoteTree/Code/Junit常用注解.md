### Junit常用注解
* @Test:将一个普通方法修饰成一个测试方法
* @Test(excepted=xx.class): xx.class表示异常类，表示测试的方法抛出此异常时，认为是正常的测试通过的
* @Test(timeout=毫秒数) :测试方法执行时间是否符合预期
* @BeforeClass： 会在所有的方法执行前被执行，static方法
* @AfterClass：会在所有的方法执行之后进行执行，static方法
* @Before：会在每一个测试方法被运行前执行一次
* @After：会在每一个测试方法运行后被执行一次
* @Ignore：所修饰的测试方法会被测试运行器忽略
* @RunWith：可以更改测试运行器org.junit.runner.Runner
* Parameters：参数化注解