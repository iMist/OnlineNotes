### 1.Git添加多个远程仓库并且完成推送
#### 添加远程仓库
* git remote add plugin https://gitee.com/iMist/Plugin.git

> 注意：plugin是本地指定的远程仓库名字

#### 查看是否添加成功
* git remote -v 

#### 查看本地分支 `git branch `

#### 将本地的某个分支推送到指定的远程仓库

* git push -f plugin dev 

> 备注：plugin是本地的远程仓库名称，dev是 本地的某个分支

###  2.拉取指定的远程分支到本地并切换 
`git checkout -b 本地分支名  origin/远程分支名`

### 3. 删除指定的本地或者远程分支
* 当我们集体进行项目时，将自定义分支push到主分支master之后，如何删除远程的自定义分支呢

1. 使用命令git branch -a 查看所有分支

2. 使用命令 git push origin --delete Chapater6   可以删除远程分支Chapater6   

3. 再次使用命令 git branch -a   可以发现，远程分支Chapater6已经被删除。

#### 删除本地分支
* `git checkout  master` 先切换到主分支;
* `git branch -d Chapater8` 可以删除本地分支（在主分支中）











