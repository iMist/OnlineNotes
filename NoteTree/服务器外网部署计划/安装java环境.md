* 检查当前
`yum list installed |grep java`
* 卸载当前
```
yum -y remove java-*-openjdk*
yum -y remove tzdata-java.noarch
```
* 查看当前的库中的版本：

`yum -y list java*`

* 同时安装openjdk和openjdk-devel即可：
`yum install java-1.8.0-openjdk  java-1.8.0-openjdk-devel`

* java -version
>openjdk version "1.8.0_131"
 OpenJDK Runtime Environment (build 1.8.0_131-b12)
 OpenJDK 64-Bit Server VM (build 25.131-b12, mixed mode)
 
 

