### Docker部署wordpress
1.拉取 wordpress 镜像
  docker pull wordpress:latest

2. 查看镜像
   docker images;
   
3. 启动容器  docker run --name=wordpress -p 8003:22 -p 8001:80 -v /var/wordpress:/data wordpress
    * 通过8001端口访问
    * 本机需要php环境
    * 暂时不清楚wordpress安装数据在那个地方
    * 注意数据库路径是本机（宿主）而不是另一个容器或者当前容器的话，需要填写本机的内网ip，而不可以是localhost或者127.0.0.1
    * 可能因为nginx配置导致样式丢失
    

#### 其他问题：docker 端口映射错误解决方法
```
docker: Error response from daemon: driver failed programming external connectivity。。。
```

* 重启docker解决 `sudo systemctl restart docker`
    
#### 样式无法正常显示
* 估计是nginx配置问题,这个就不深究了


### 最终使用和typecho一样使用站点的方式配置
* 安装php环境,下载压缩包,解压到 `/var/www/html`这个和typecho一样就不多说了 ,需要注意的是目录目录权限问题

* 安装配置mysql和NGINX

* 相关配置参考`typecho`的配置

### 贴一段NGINX的配置
```nginx
server {
        listen          80;
        server_name     www.imisty.cn imisty.cn;
        root            /var/www/html/wordpress;
        index           index.html index.htm index.php;
        client_max_body_size 100M;

        if (!-e $request_filename) {
            rewrite ^(.*)$ /index.php$1 last;
        }

        location ~ .*\.php(\/.*)*$ {
            include fastcgi.conf;
            fastcgi_index  index.php;
            fastcgi_pass  127.0.0.1:9000;

            fastcgi_connect_timeout 300;

            fastcgi_send_timeout 300;

            fastcgi_read_timeout 300;
        }


        error_log  logs/error_wordpress.log;
        access_log logs/misty.log combined;
    }
```

### 常见问题解决方案
* 英文包转换成中文环境,在站点根目录找到wp-config.php,将`define('WPLANG', '');`改为`define('WPLANG', 'zh_CN');`没有则添加即可,然后刷新控制台面板出现更新直接下载中文包即可
* 下载包需要验证ftp,编辑`wp-config.php`如下即可,官方建议升级完成之后修改回来

```
define('WPLANG', 'zh_CN');
define("FS_METHOD", "direct");
define("FS_CHMOD_DIR", 0777);
define("FS_CHMOD_FILE", 0777);

```

* 解决“在裁剪您的图像时发生了错误”   php-gd或者相关扩展没有安装吧,官方论坛搜索即可 
    
    
    
    
### 打开域名访问主页变成下载`index.php`页面的源代码，其他路径的页面可以正常访问或者访问域名跳转到127.0.0.1
出现这个问题,就有点懵了，毕竟NGINX的配置没有怎么改，之前都是好好地，但是配置文件更换频繁导致的，浏览器客户端不解析php,回家访问一次发现解决了这个问题怀疑是浏览器缓存导致的,浏览器记住了之前的配置不会解析php成html

解决方案:这个是因为浏览器缓存的原因导致的，清除浏览器缓存即可    
    
### 连续刷新页面出现502 错误，php-cgi程序崩溃了

##### 照抄网上的解决方案
* 查看当前的PHP FastCGI进程数是否够用，如果实际使用的“FastCGI进程数”接近预设的“FastCGI进程数”，那么，说明“FastCGI进程数”不够用，需要增大

`netstat -anpo | grep "php-cgi" | wc -l`

* 部分PHP程序的执行时间超过了Nginx的等待时间，可以适当增加nginx.conf配置文件中FastCGI的timeout时间，例如：

```
http

{

......

fastcgi_connect_timeout 300;

fastcgi_send_timeout 300;

fastcgi_read_timeout 300;

......
 
}
```      

* php.ini中memory_limit设低了会出错，修改了php.ini的memory_limit为64M，重启nginx，发现好了，原来是PHP的内存不足了      

>这样多次刷新依然出现502错误

```
Nginx错误日志

recv() failed (104: Connection reset by peer) while reading response header from upstream, client: 183.54.41.123, server: www.imisty.cn, request: "GET /index.php?rest_route=/yoast/v1/statistics HTTP/1.1", upstream: "fastcgi://127.0.0.1:8001", host: "imisty.cn", referrer: "http://imisty.cn/wp-admin/"

```


### 用`php-fpm`服务进行管理,解决多次刷新页面502的问题
* `yum install php70w-fpm` 安装,因为之前安装过旧版本，存在冲突,这里需要指定版本
* `service  php-fpm restart` 启动php-fpm服务

>lsof查看php-cgi端口kill掉；php-fpm默认的端口是9000





### 上传的文件尺寸超过php.ini中定义的upload_max_filesize值。

* 修改`/etc/php.ini`中的属性值即可