#### 转载自 <http://www.imooc.com/article/18794?block_id=tuijian_wz>

### 环境搭建步骤
* 安装docker `yum -y install docker`

* 启动docker服务 `service docker start`

* 从docker镜像库拉取gogs `docker pull gogs/gogs`

* 创建相应的文件夹 `mkdir -p /var/gogs`

* 创建docker容器gogs `docker run --name=gogs -p 10022:22 -p 10080:3000 -v /var/gogs:/data gogs/gogs`

#### 安装
* 访问地址：自己服务器ip+:10080

* 如下图域名记得填写本机ip【不要填写127.0.0.1或localhost】端口号10080（本文最下面有提到相关问题问题）

![](/images/image15.png)

![](/images/image20.png)

* 添加管理员账户

* 最后我们就可以访问了：自己服务器ip+:10080
  
#### 相关命令
* docker start gogs 在容器中启动Gogs

* docker ps 列出容器

##### Gogs——clone仓库地址为本地localhost的问题

* 在自己服务器上搭建过Gogs之后我发现一个问题，我无法正确的获取到clone的地址，地址栏上显示的是localhost并不是服务器的ip地址，而且端口号还不对 不是10080  详见下图
  
##### 解决方案
1. 进入docker `docker exec -it gogs /bin/bash   #前提是docker start gogs`

2. 修改配置文件 
    * `cd ~/../data/gogs/conf/`   进入到相应配置文件夹 
    * `vi app.ini `   编辑配置文件 
    ![](/images/image16.png)
   
   >在这里我们只需要修改[server]下面的DOMAIN和ROOT_URL把localhost改为服务器主机ip

    * `exit   退出docker容器`
      
    *  `docker restart gogs   重启`
    
    
### 用nginx配置服务push 代码出现如下错误
* `error: RPC failed; result=22, HTTP code = 413 `

* 修改nginx文件上传配置,`client_max_body_size 100M;`

```
server {
listen 80;
autoindex on;
server_name git.imisty.cn;
# 限制单个文件上传大小，或者采用ssl协议
client_max_body_size 100M;
access_log /usr/local/nginx/logs/access.log combined;
index index.html index.htm index.jsp index.php;
#root /devsoft/apache-tomcat-7.0.73/webapps/mmall;
#error_page 404 /404.html;
if ( $query_string ~* ".*[\;'\<\>].*" ){
        return 404;
        }
location / {
        proxy_pass http://127.0.0.1:10080/;
        add_header Access-Control-Allow-Origin '*';
        }
}
```

### 还有一个小坑,创建容器的时候同时也启动了容器，此时是在前台运行的，按下Ctrl+C就会中断
* 重新启动容器即可，关注容器的运行状态就很容易发现
    
      
    
    
      
           
    
    
    
 

 



  
  
