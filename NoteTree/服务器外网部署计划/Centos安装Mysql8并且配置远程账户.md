### MySql 8 安装
#### 安装须知
>开始之前我们需要链接到服务器，如何链接呢？答案是使用SSH，Mac系统自带SSH，而Windows需要先安装SSH工具，这个百度就好，另外Windows10似乎自带了Ubuntu Bash，这个东西也就是一个小型Linux系统，可以直接使用SSH命令，详细的开启方式百度：“Windows10 Ubuntu Bash”。

* 配置好SSH环境之后，连接到指定服务器
```
ssh -l root *.*.*.*  //后面是你的服务器外网ip，或者写你映射的域名
ssh -p 488 -l root *.*.*.*  //如果更改过默认端口，则为这种方式
```
* 成功登陆服务器 `Welcome to Alibaba Cloud Elastic Compute Service !`

#### 登陆服务器
* 要进行MySQL安装，我们需要具备 yum 工具，这个CentOS自带就有，我们需要的是升级一下所有的库版本即可。

```
#yum check-update     // 检查
#yum -y update    // 更新
```

#### 下载MySql相关包
* 进入官网：<https://dev.mysql.com/downloads/repo/yum/>
* 点击下载获取地址在服务器通过命令下载 
    ![](/images/image8.png)
* Copy链接地址，在云服务器中使用命令下载：下载到当前目录`wget https://repo.mysql.com//mysql57-community-release-el7-11.noarch.rpm`
 
* 下载后在当前目录使用ls命令查询是否下载成功。如果有必要使用如下命令校验md5信息：   
`md5sum mysql57-community-release-el7-9.noarch.rpm`

#### 安装下载的源
   * 两种安装方式任选一种即可 `sudo rpm -ivh mysql57-community-release-el7-11.noarch.rpm ` or `sudo yum localinstall mysql57-community-release-el7-11.noarch.rpm `
   
#### 检查安装列表   
```
// 该操作无须进行，直接下一步，用于检查错误使用
 cd /etc/yum.repos.d/     //进入仓库列表
 ls //查看当前仓库是否安装了mysql
 ```
 * 如果安装成功就可以看到安装列表了
 `yum repolist all | grep mysql`
 
 * 后面为enable的为默认可安装的，你可以切换（默认无须更改）；其实也就是定义一下mysql的安装组件：
 ```
 sudo yum-config-manager --disable mysql57-community
 sudo yum-config-manager --enable mysql56-community
 ```
 ![](/images/image9.png)
 
 #### 安装mysql
 * 执行安装命令安装即可(安装仓库的安装包)，遇到需要同意的地方,输入y同意即可
 `sudo yum install mysql-community-server`
 
 #### 启动mysql
 ```
 sudo service mysqld start  //启动
 sudo service mysqld stop  //停止
 sudo service mysqld status  //查看状态
 sudo service mysqld restart  //重启
 或者
 sudo systemctl start mysqld  //启动
 sudo systemctl stop mysqld  //停止
 sudo systemctl status mysqld  //查看状态
 sudo systemctl restart mysqld //重启
 ```
 
 #### 开机启动 
 ```
  systemctl enable mysqld
  systemctl daemon-reload
  ```
  
  #### 安装完成开始配置
  * 第一步，安装完成后一般而言已经有本地root账户了，这里要问了，我擦密码我都不知道怎么就有账户了，真的有了，有了；好吧我们这样拿到密码：
  
   `sudo grep 'temporary password' /var/log/mysqld.log`
  
  * 执行该命令后，在末尾可以看见密码：
  ![](/images/image10.png)
  
  * 知道密码后我们需要紧急修改，请记住修改后将不能使用这个方式拿到密码了。首先我们需要登陆：
  
  `mysql -u root -p`
  
  * 请输入上述的密码进入mysql，进入后窗口前面会变成:mysql。
  
  * 修改密码
  ```
  mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyNewPass4!';
  // 或者
  mysql> set password for 'root'@'localhost'=password('MyNewPass4!');
  ```
  * 注意：密码需要包含：大小写字母+数字+字符组合。
    
 >   完了？？真完啦？？没呢，默认root账户并不能使用与远程链接，所以我们要么打开权限，要么新增账户。

#### 新增远程账户 
 `mysql> GRANT ALL PRIVILEGES ON *.* TO 'iMisty'@'%' IDENTIFIED BY 'MyNewPass4!' WITH GRANT OPTION;`
 * iMisty 账户名
 * MyNewPass4! 密码 ，大小写字母+数字+字符组合
>如上操作会出现语法错误,详情参考下面的解决方案

*  如果你想允许用户user从ip为192.168.1.4的主机连接到mysql服务器，并使用mypwd作为密码

    * GRANT ALL PRIVILEGES ON *.* TO 'user'@'192.168.1.4' IDENTIFIED BY 'mypwd' WITH GRANT OPTION;  

 
#### 注意的地方
 * mysql命令行退出 `exit`命令, mysql一句语句执行以分号结尾（默认）
 * mysql8和原来的版本有点不一样，8的安全级别更高，所以在创建远程连接用户的时候，不能用原来的命令（同时创建用户和赋权）:


### 其他相关命令；
* 查询MySQL的版本 select version(); 
* 使用指定的数据库 use mysql;

### 以下就是mysql8 的新增远程账户的步骤


### 用户登陆
1. 登陆mysql `mysql -u root -p`
2. 选择MySQL数据库 `use mysql;`
3. 在 mysql 数据库的 user 表中查看当前 root 用户的相关信息 
`select host, user, authentication_string, plugin from user;`

*  Mysql5.7 可以允许创建用户的同时赋予权限; localhost 代表允许本地访问 `%`代表允许远程访问  
`mysql> GRANT ALL PRIVILEGES ON *.* TO 'uahello'@'%' IDENTIFIED BY 'MyNewPass4!' WITH GRANT OPTION;`
>如上SQL语句在Mysql8会出现语法错误;

#### Mysql8 之后的创建远程连接账户的方式
1. 必须先创建用户（密码规则：mysql8.0以上密码策略限制必须要大小写加数字特殊符号）：

    * 允许用户远程连接 `mysql>create user 'username'@'%' identified  by 'password';`密码必须数字+大小写字母+字符
    * 不允许用户远程连接（本地用户） `mysql>create user 'username'@'localhost' identified  by 'password';`密码必须数字+大小写字母+字符

* 更新允许远程连接 ,如果第一步有远程连接的host可以不用更新`UPDATE user SET host = '%' WHERE user = 'username';`

2. 再进行权限赋值：

 赋予用户所有权限（全部数据库的全部权限）`mysql>grant all privileges on *.* to chenadmin@'%' with grant option;`
```
#这里可能会因为mysql8的加密规则无法修改权限,需要先修改加密规则
mysql> grant all privileges on *.* to misty@'%' with grant option;
ERROR 1410 (42000): You are not allowed to create a user with GRANT
mysql> ALTER USER 'misty'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
ERROR 1819 (HY000): Your password does not satisfy the current policy requirements
mysql> ALTER USER 'misty'@'%' IDENTIFIED WITH mysql_native_password BY 'iMisty6!';
Query OK, 0 rows affected (0.01 sec)

mysql> grant all privileges on *.* to misty@'%' with grant option;
Query OK, 0 rows affected (0.01 sec)

```

3.最后刷新一下,不然不会立即生效

`mysql>flush privileges;`

![](/images/image21.png)

*  当你进行远程连接是，会出现这样的错误:(实际操作过程中的过程中没有出现)



`Unable to load authentication plugin 'caching_sha2_password'.`

* 是因为mysql8使用的是caching_sha2_password加密规则，最简单的方法是修改远程连接用户的加密规则：
![img](https://gitee.com/iMist/res/raw/master/2019/9/29/5c197892-2696-44a5-a1ee-65fcef987f24.png)

`mysql>ALTER USER 'username'@'%' IDENTIFIED WITH mysql_native_password BY 'password';`

### 开放mysql 防火墙端口并且添加阿里云服务器的安全组规则
* `sudo firewall-cmd --zone=public --add-port=3306/tcp --permanent`
* `sudo firewall-cmd --reload`

此时就可以通过外网访问mysql服务了;