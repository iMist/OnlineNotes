### 异常代码
`java.sql.SQLException: Unable to load authentication plugin 'caching_sha2_password'.`

##### 网上查资料说的是mysql5.x 版本和 8.x版本的区别；

* 5.7版本是：default_authentication_plugin=mysql_native_password

* 8.x版本就是：default_authentication_plugin=caching_sha2_password

>尝试用过`default_authentication_plugin=mysql_native_password;`
以及`ALTER USER root@localhost IDENTIFIED WITH mysql_native_password BY ‘11111111’;`毫无作用甚至报错

##### 最终解决方案
* mysql驱动已经更新适配了`caching_sha2_password `的密码规则，升级到最新版本就可以了。

* gradle 代码 `compile group: 'mysql', name: 'mysql-connector-java', version: '8.0.11'`


### 数据库连接阻塞
* `Host is blocked because of many connection errors;unblock with 'mysqladmin flush-hosts'`
    
> 原因：同一个ip在短时间内产生太多（超过mysql数据库max_connection_errors的最大值）中断的数据库连接而导致的阻塞；
    
* 解决方法：我只说治本的,进入到数据库中 执行下面的命令
    
 `flush hosts;`
> 但是实际执行之后有好转，但是在同样的操作情况下，重启服务器，还是会出现相同的问题;
   
