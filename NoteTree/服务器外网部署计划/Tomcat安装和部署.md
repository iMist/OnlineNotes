### 安装Tomcat
>在有Java8的基础上我们可以开始配置咱们的Tomcat啦～～

* 与上面Java一样，咱们进行一次旧版本移除；默认新服务器无须做如下步骤：

```
yum list installed |grep tomcat
yum -y remove tomcat*
```
> 实际操作中的移除 sudo yum remove tomcat 正确删除

* 查看一下版本
`yum -y list tomcat*`
* 咱们一共需要安装的有3项（你可以分开一个个来）：
`sudo yum install tomcat tomcat-webapps tomcat-admin-webapps `
* 如果你需要文档，那么可以执行：
` sudo yum install tomcat-docs-webapp tomcat-javadoc`
* 完成以上步骤之后你可以查看版本了：`tomcat version`

>Server version: Apache Tomcat/7.0.69
 Server built: Apr 12 2017 23:39:01 UTC
 Server number: 7.0.69.0
 OS Name: Linux
 OS Version: 3.10.0-514.21.2.el7.x86_64
 Architecture: amd64
 JVM Version: 1.8.0_131-b12
 JVM Vendor: Oracle Corporation
 
 * 请记住，现在无须启动，因为咱们还有配置。这里仅仅放出相关命令：
 
 ```
 sudo systemctl start tomcat   //启动
 sudo systemctl restart tomcat  //重启
 sudo systemctl stop tomcat   //停止
 sudo service tomcat status // 查看状态
 ```
 >在上面的命令中，也有两种写法，所以我混合写了；你们可以参照MySQL的方式。
 
 
 ### 配置并且部署Tomcat 
 
*  首先我们配置Tomcat Web管理界面：
 `sudo vi /usr/share/tomcat/conf/tomcat-users.xml`
 
*  英文输入法情况下按 i 进入编辑状态，在非注释空白地方回车加入(`<tomcat-users>`节点下)：

` <user username="admin" password="adminpwd" roles="manager-gui,admin-gui"/>`
>以上操作配置了Tomcat Web管理员的账户和密码以及可管理的权限,在访问管理界面的时候会弹出验证的对话框。

* 紧接着按`Esc`进入退出操作，`shift+q` 进入退出操作，此时输入 `:wq` 保存并退出；回车即可完成。

* 此时你可以启动Tomcat，并在你自己电脑使用浏览器访问你的服务器了。
`http://ip_address:8080`

* 默认端口为8080。你还可以进入管理界面：
`http://ip_address:8080/manager/html`

* 输入上面配置的用户名和密码即可进入管理界面

>首次访问的时间可能会有点慢，修改配置之后需要重启服务生效

#### 开机启动
 ```   
  sudo systemctl enable tomcat
  sudo systemctl daemon-reload
  ```   
> 以上命令即可把tomcat配置为开机启动，重启服务器后无须自己手动操作。


### 重点: 配置项目

* 如果你上面的配置OK,那么你可以进入到项目配置了.

>项目配置有两种，一种是仅仅更改端口，然后把项目代码替换当前默认的管理界面，这种方式简单，但是侵入性太大，不建议使用，因为可能以后你还需要再次使用到管理页面。
那么另外一种就是独立的配置一个项目区间进去，该方式与管理页面并行，通过不同端口访问服务器，甚至你可以定义为同样的端口，但是使用域名进行区分。该方式配置复杂，但是是一种较优秀的方案。

* 本文章所使用的方式为第二种方案。

```xml
<Service name="Catalina-italker">
    <Connector port="8083" protocol="HTTP/1.1" connectionTimeout="20000" redirectPort="8443" />
    <Connector port="8689" protocol="AJP/1.3" redirectPort="8443" />
    <Engine name="Catalina" defaultHost="localhost">
        <Realm className="org.apache.catalina.realm.LockOutRealm">
            <Realm className="org.apache.catalina.realm.UserDatabaseRealm" resourceName="UserDatabase" />
        </Realm>
        <Host name="localhost" appBase="webapps-italker" unpackWARs="true" autoDeploy="true">
            <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs" prefix="italker_access_log." suffix=".txt" pattern="%h %l %u %t &quot;%r&quot; %s %b" />
        </Host>
    </Engine>
</Service>
```
* 我们来简单讲解一下相关的意思:

![](/images/image18.png)

* 开始编辑 `vim /usr/share/tomcat/conf/server.xml`

* 把咱们上面的代码插入到 service 与 server 之间即可；你可以配置多个不同的 service 。
![](/images/28244218-96a1f086-6a16-11e7-9a6e-0a83c151c715.png)

* 回到tomcat目录，并显示目录，然后执行命令创建文件夹 webapps-italker 
    * `mkdir webapps-italker`
    
* push项目到远程服务器，然后将war包放入创建的webapps-italker 目录下
    * `scp  ROOT.war  root@192.168.30.109:~  `  

    
#### 再说一种简单的方式，直接将打好的war包通过tomcat管理页面上传到服务器点击 deploy即可完成部署

```
<property name="connection.driver_class">com.mysql.cj.jdbc.Driver</property>
        <!--链接地址用户名密码 -->
        <!--<property name="connection.url">jdbc:mysql://127.0.0.1:3306/DB_I_T_PUSH_TEST?serverTimezone=UTC&amp;useUnicode=true&amp;characterEncoding=utf8&amp;useSSL=false</property>
        <property name="connection.username">root</property>
        <property name="connection.password">123456</property>-->

        <!--这种方式无法访问，没有看日志，猜想就算是服务器对本地数据库而言也是localhost,用远程的反而弄巧成拙-->
        <!--<property name="connection.url">jdbc:mysql://imisty.cn:3306/DB_I_T_PUSH_TEST?serverTimezone=UTC&amp;useUnicode=true&amp;characterEncoding=utf8&amp;useSSL=false</property>
        <property name="connection.username">misty</property>
        <property name="connection.password">iMisty6!</property>-->
        
        <!--最终的部署的配置代码，若是数据库在本机的话直接用localhost即可，用远程的域名反而会出问题-->
        <property name="connection.url">jdbc:mysql://localhost:3306/DB_I_T_PUSH_TEST?serverTimezone=UTC&amp;useUnicode=true&amp;characterEncoding=utf8&amp;useSSL=false</property>
        <property name="connection.username">misty</property>
        <property name="connection.password">iMisty6!</property>
 ```
 
 #### 贴一段比较标准的配置文件
 
 ```xml
<?xml version='1.0' encoding='utf-8'?>
    <!--
      Licensed to the Apache Software Foundation (ASF) under one or more
      contributor license agreements.  See the NOTICE file distributed with
      this work for additional information regarding copyright ownership.
      The ASF licenses this file to You under the Apache License, Version 2.0
      (the "License"); you may not use this file except in compliance with
      the License.  You may obtain a copy of the License at
    
          http://www.apache.org/licenses/LICENSE-2.0
    
      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
    -->
    <!-- Note:  A "Server" is not itself a "Container", so you may not
         define subcomponents such as "Valves" at this level.
         Documentation at /docs/config/server.html
     -->
     <!--监听tomcat关闭的端口-->
    <Server port="8005" shutdown="SHUTDOWN">
      <Listener className="org.apache.catalina.startup.VersionLoggerListener" />
      <!-- Security listener. Documentation at /docs/config/listeners.html
      <Listener className="org.apache.catalina.security.SecurityListener" />
      -->
      <!--APR library loader. Documentation at /docs/apr.html -->
      <Listener className="org.apache.catalina.core.AprLifecycleListener" SSLEngine="on" />
      <!--Initialize Jasper prior to webapps are loaded. Documentation at /docs/jasper-howto.html -->
      <Listener className="org.apache.catalina.core.JasperListener" />
      <!-- Prevent memory leaks due to use of particular java/javax APIs-->
      <Listener className="org.apache.catalina.core.JreMemoryLeakPreventionListener" />
      <Listener className="org.apache.catalina.mbeans.GlobalResourcesLifecycleListener" />
      <Listener className="org.apache.catalina.core.ThreadLocalLeakPreventionListener" />
    
      <!-- Global JNDI resources
           Documentation at /docs/jndi-resources-howto.html
      -->
      <GlobalNamingResources>
        <!-- Editable user database that can also be used by
             UserDatabaseRealm to authenticate users
        -->
        <Resource name="UserDatabase" auth="Container"
                  type="org.apache.catalina.UserDatabase"
                  description="User database that can be updated and saved"
                  factory="org.apache.catalina.users.MemoryUserDatabaseFactory"
                  pathname="conf/tomcat-users.xml" />
      </GlobalNamingResources>
    
      <!-- A "Service" is a collection of one or more "Connectors" that share
           a single "Container" Note:  A "Service" is not itself a "Container",
           so you may not define subcomponents such as "Valves" at this level.
           Documentation at /docs/config/service.html
       -->
      <Service name="Catalina">
        <!--The connectors can use a shared executor, you can define one or more named thread pools-->
        <!--
        <Executor name="tomcatThreadPool" namePrefix="catalina-exec-"
            maxThreads="150" minSpareThreads="4"/>
        -->
        <!-- A "Connector" represents an endpoint by which requests are received
             and responses are returned. Documentation at :
             Java HTTP Connector: /docs/config/http.html (blocking & non-blocking)
             Java AJP  Connector: /docs/config/ajp.html
             APR (HTTP/AJP) Connector: /docs/apr.html
             Define a non-SSL HTTP/1.1 Connector on port 8080
        -->
     <Connector port="8080"  protocol="HTTP/1.1"
                   connectionTimeout="20000"
                   redirectPort="8443" />
    <!-- A "Connector" using the shared thread pool-->
        <!--
        <Connector executor="tomcatThreadPool"
                   port="8080" protocol="HTTP/1.1"
                   connectionTimeout="20000"
                   redirectPort="8443" />
        -->
        <!-- Define a SSL HTTP/1.1 Connector on port 8443
             This connector uses the BIO implementation that requires the JSSE
             style configuration. When using the APR/native implementation, the
             OpenSSL style configuration is required as described in the APR/native
             documentation -->
        <!--
        <Connector port="8443" protocol="org.apache.coyote.http11.Http11Protocol"
                   maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
                   clientAuth="false" sslProtocol="TLS" />
        -->
        <!-- Define an AJP 1.3 Connector on port 8009 -->
        <Connector port="8081" protocol="AJP/1.3" redirectPort="8443" />
        <!-- An Engine represents the entry point (within Catalina) that processes
             every request.  The Engine implementation for Tomcat stand alone
             analyzes the HTTP headers included with the request, and passes them
             on to the appropriate Host (virtual host).
             Documentation at /docs/config/engine.html -->
    
        <!-- You should set jvmRoute to support load-balancing via AJP ie :
        <Engine name="Catalina" defaultHost="localhost" jvmRoute="jvm1">
        -->
        <Engine name="Catalina" defaultHost="imisty.cn">
          <!--For clustering, please take a look at documentation at:
              /docs/cluster-howto.html  (simple how to)
              /docs/config/cluster.html (reference documentation) -->
          <!--
          <Cluster className="org.apache.catalina.ha.tcp.SimpleTcpCluster"/>
          -->
          <!-- Use the LockOutRealm to prevent attempts to guess user passwords
               via a brute-force attack -->
          <Realm className="org.apache.catalina.realm.LockOutRealm">
            <!-- This Realm uses the UserDatabase configured in the global JNDI
                 resources under the key "UserDatabase".  Any edits
                 that are performed against this UserDatabase are immediately
                 available for use by the Realm.  -->
            <Realm className="org.apache.catalina.realm.UserDatabaseRealm"
                   resourceName="UserDatabase"/>
          </Realm>
          <Host name="imisty.cn"  appBase="webapps"
                unpackWARs="true" autoDeploy="true">
            <!-- SingleSignOn valve, share authentication between web applications
                 Documentation at: /docs/config/valve.html -->
            <!--
            <Valve className="org.apache.catalina.authenticator.SingleSignOn" />
            -->
            <!-- Access log processes all example.
                 Documentation at: /docs/config/valve.html
                 Note: The pattern used is equivalent to using pattern="common" -->
            <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
                   prefix="localhost_access_log." suffix=".txt"
                   pattern="%h %l %u %t &quot;%r&quot; %s %b" />
     </Host>
        </Engine>
      </Service>
    
    <!--iTalker Config-->
    <Service name="Catalina-italker">
    <Connector port="8083" protocol="HTTP/1.1" connectionTimeout="20000" redirectPort="8443" />
        <Connector port="8084" protocol="AJP/1.3" redirectPort="8443" />
        <Engine name="Catalina" defaultHost="localhost">
            <Realm className="org.apache.catalina.realm.LockOutRealm">
                <Realm className="org.apache.catalina.realm.UserDatabaseRealm" resourceName="UserDatabase" />
            </Realm>
            <Host name="localhost" appBase="webapps-italker" unpackWARs="true" autoDeploy="true">
                <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs" prefix="italker_access_log." suffix=".txt" pattern="%h %l %u %t &quot;%r&quot; %s %b" />
            </Host>
        </Engine>
    </Service>
    
</Server>

```

##### 重启tomcat查看状态
```
sudo systemctl restart tomcat  //重启
sudo service tomcat status // 查看状态
```
>如果有正在运行的状态提示，那么恭喜你,到此我们的项目就算配置完成了。

> 注意事项 : 现在你可以在自己电脑通过服务器IP+端口的方式来访问我们的项目了，首次访问时间会稍微长一些，看服务器配置了。
但是配置完成，不一定可以马上访问，这个取决于服务器的配置；因为我的服务器配置较低，可能需要很久才能成功启动并且部署完成
所以检查配置项和日志输出没有问题，静静等待喝杯java就OK了







