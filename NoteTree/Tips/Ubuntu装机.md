
### 首先安装主题和图标
* 安装 tweaks，使用该软件统一管理主题中的各个部分`sudo apt install gnome-tweak-tool `

* 安装一款 google 的material design的主题`vimix-gtk-themes`

* 拉取源码使用安装脚本即可 
    * `git clone https://github.com/vinceliuice/vimix-gtk-themes.git`
    * `git clone https://github.com/vinceliuice/vimix-icon-theme.git`
    
* 推荐一款mac主题
    * `https://github.com/vinceliuice/Sierra-gtk-theme `   

* 然后在tweaks 里面设置

>注意，如果安装之后无法发现需要注销当前账户重新登陆

### 主题的安装方法
* 主题的安装方法 `https://blog.csdn.net/ksws0292756/article/details/79953155`



### 双系统时间同步
* 先在ubuntu下更新一下时间，确保时间无误
    * `sudo apt install ntpdate`
    * `sudo ntpdate time.windows.com`
* 然后将时间更新到硬件上
    * `sudo hwclock --localtime --systohc`    

### 安装idea 
* 下载idea 解压
* 进入解压目录执行安装脚本即可

### 安装Java 和 Android SDK并且配置环境变量
* java官网安装直接解压包即可
* SDK 通过idea下载 

* 编辑`/etc/profile` 文件,添加如下环境变量
    * 编辑 `sudo gedit ~/.bashrc` 设置用户变量

```
export JAVA_HOME=/home/misty/MyFile/jdk1.8.0_211
 
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar

export PATH=$PATH:$JAVA_HOME/bin:

# android sdk
export ANDROID_HOME=/home/misty/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/platform-tools

# android ndk
export ANDROID_NDK=/home/sky/android/android-ndk(NDK配置目录)
export ANDROID_NDK
```

* 执行`source /etc/profile` 环境变量生效

### gradle 环境配置
* 下载地址 `http://services.gradle.org/distributions/`

* 解压 `unzip   gradle-4.4-all.zip `




### 安装网易云音乐
* 直接官网下载即可


### 安装搜狗输入法
* 参考文章 : `https://www.jianshu.com/p/c936a8a2180e`

* Ctrl+shift+F 繁简切换
>最后还是采用ubuntu18默认的输入法

### Ubuntu 18亮度调节---让小太阳显形！

![](/images/image19.png)

* 看右上角，有音量调节的小喇叭图标和亮度调节的小太阳图标了吧。我刚开始是只有喇叭图标的。那么如何让太阳显形呢？

1.首先Ctrl+Alt+T调出一个命令行界面来。

2.执行这个命令：`sudo nano /etc/default/grub`   它会打开一个叫grub的文件

3.将  GRUB_CMDLINE_LINEX_DEFAULT那一行改成：

`GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=vendor acpi_osi=Linux"`

```
将GRUB_CMDLINE_LINUX=""改为GRUB_CMDLINE_LINUX="acpi_backlight=vendor" 

或者使用下面更复杂的配置

GRUB_CMDLINE_LINUX_DEFAULT="quiet splash" 
GRUB_CMDLINE_LINUX=""

修改为

acpi_osi=Linux acpi_backlight=vendor
GRUB_CMDLINE_LINUX_DEFAULT=”quiet splash acpi_osi=Linux”
GRUB_CMDLINE_LINUX="acpi_backlight=vendor“

```
4. 按 Ctrl+O 保存。

5. 按 Ctrl+X 退出编辑。

6. 输入并执行 `sudo update-grub`。

7. 重启机子。

* 按理说重启以后，小太阳应该就出来啦.
>重启之后无效，最后用显示屏的亮度调节吧，不深究了


### 安装crossover 
*使用终端,开启32bit支持
    * `sudo dpkg –add-architecture i386 ; sudo apt-get update`

* 下载程序解压,会获得*.dep和*.rpm以及*.bin安装脚本

*  安装包程序安装 dpkg -i *.dep 

>一般情况下，.bin 安装程序是通用的，可以在任何 Linux 系统安装。但是这个安装程序不会下载任何依赖包，所以我们强烈建议您首先使用适合您系统的 .deb 包或 .rpm 包进行安装

* 赋予 `*.bin`执行权限,执行安装程序

* 常见问题解决方式 
    * 检查数据源配置,清除无效的数据源
    * 依赖库问题使用aptitude 解决相关依赖


### 卸载软件
* 查看软件安装列表 `dpkg --list |grep cross`
* 卸载软件 `sudo apt-get purge crossover:i386`

```
misty@ubuntu:~/下载/crossover_linux_18.5.5$ dpkg --list |grep cross
ii  crossover:i386                             18.5.0-1                                     i386         Run Windows applications like MS Office
ii  libieee1284-3:amd64                        0.2.11-13                                    amd64        cross-platform library for parallel port access
misty@ubuntu:~/下载/crossover_linux_18.5.5$ sudo apt-get purge crossover:i386
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
下列软件包是自动安装的并且现在不需要了：
  libavahi-client3:i386 libavahi-common-data:i386 libavahi-common3:i386
  libcups2:i386 libdbus-1-3:i386 libdrm-amdgpu1:i386 libdrm-intel1:i386

```

* apt-get purge / apt-get --purge remove 
    删除已安装包（不保留配置文件)。 
    如软件包a，依赖软件包b，则执行该命令会删除a，而且不保留配置文件

* apt-get autoremove 
    删除为了满足依赖而安装的，但现在不再需要的软件包（包括已安装包），保留配置文件。

* apt-get remove 
    删除已安装的软件包（保留配置文件），不会删除依赖软件包，且保留配置文件。

* apt-get autoclean 
    APT的底层包是dpkg, 而dpkg 安装Package时, 会将 *.deb 放在 /var/cache/apt/archives/中，apt-get autoclean 只会删除 /var/cache/apt/archives/ 已经过期的deb。

* apt-get clean 
    使用 apt-get clean 会将 /var/cache/apt/archives/ 的 所有 deb 删掉，可以理解为 rm /var/cache/apt/archives/*.deb。



### 因为您要求某些软件包保持现状，就是它们破坏了软件包间的依赖关系
* sudo apt-get update
* sudo apt-get install aptitude

* sudo aptitude install ...






