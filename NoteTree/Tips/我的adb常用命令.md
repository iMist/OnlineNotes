### 查看当前的app应用界面
 * `adb shell "dumpsys window | grep mCurrentFocus"`

### adb无线调试详细步骤
* `adb tcpip 6668 ` 开放监听指定的端口，可以随便指定，默认是5555

* `adb shell ifconfig wlan0  ` 查看手机的IP地址

* `adb connect ip:6668` 无线连接手机,默认端口可以不用端口号
>无线和USB连接是不冲突的，一部手机可以同时存在两个连接；多部手机可以无线连接同一端口也可以连接不同的端口，前提是有该端口的监听;
##### 多台设备的连接
*  可以同时使用USB和无线连接手机，但是无线连接需要先开启无线连接，开放指定的端口

* `adb -s [设备名] tcpip 6667`重新为该设备指定一个端口

##### 其他的相关命令
* `adb disconnect ip地址`  断开连接
* `adb usb` 切换回USB连接
* `adb -s emulator-5554 install xxx.apk `指定设备安装apk
