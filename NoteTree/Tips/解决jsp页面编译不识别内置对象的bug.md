
### 在jsp页面无法识别内置对象，是因为编译期间没有相应的包，而在运行期，Tomcat有，所以需要在开发阶段引入相关的jar
```
<dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.0</version>
            <scope>provided</scope>
        </dependency>
        <!-- https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
            <scope>provided</scope>
        </dependency>



        <!-- 或者采用 jsp-api、servlet-api、el-->
        <dependency>
      <groupId>org.apache.tomcat</groupId>
      <artifactId>tomcat-jsp-api</artifactId>
      <version>8.5.5</version>
      <scope>provided</scope>
    </dependency>
```        