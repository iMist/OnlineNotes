#### windows 子系统 linux （WLS）： ifconfig ping等无法使用解决办法

#### 解决办法
1. 更改更新源为 16.04 (xenial)
   方法一：修改初始的更新源文件
   备份初始的源文件为sources.list.ORIG，将sources.list里的"trusty"全替换为"xenial"。$ sed -i.ORIG 's/trusty/xenial/g' /etc/apt/sources.list
2.方法二（推荐）：把更新源直接改为国内的阿里云Ubuntu(xenial)镜像，这样会很快
   编辑更新源文件
   `$ vim /etc/apt/sources.list`
   备份源文件后粘贴如以下代码，并保存退出。
   
```
   deb http://mirrors.aliyun.com/ubuntu/ xenial main restricted universe multiverse
   deb http://mirrors.aliyun.com/ubuntu/ xenial-security main restricted universe multiverse
   deb http://mirrors.aliyun.com/ubuntu/ xenial-updates main restricted universe multiverse
   deb http://mirrors.aliyun.com/ubuntu/ xenial-proposed main restricted universe multiverse
   deb http://mirrors.aliyun.com/ubuntu/ xenial-backports main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ xenial main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ xenial-security main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ xenial-updates main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ xenial-proposed main restricted universe multiverse
   deb-src http://mirrors.aliyun.com/ubuntu/ xenial-backports main restricted universe multiverse
```

>如果/etc/apt/sources.list.d/这个文件夹不能空，需要备份下，空的话就不需要备份。
 
1. ` mv /etc/apt/sources.list.d/ /etc/apt/sources.list.d.back/`

2. `mkdir /etc/apt/sources.list.d/`

#### 升级至 16.04 (xenial)
   `aptitude update`
   `aptitude safe-upgrade -y`
   
>  注1：升级过程中会提示你重启服务（restart services），选yes;
   注2：还会出现文件冲突，保留当前版本（current version）即可，输入N;

* `apt dist-upgrade`
* `apt install aptitude`
* `apt autoremove `

#### 以上都成功之后,之后就可以用使用网络命令了！
# lsb_release -a
```
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 16.04.3 LTS
Release:        16.04
Codename:       xenial
```

>实际上如上配置也没有办法访问linux网络服务;


