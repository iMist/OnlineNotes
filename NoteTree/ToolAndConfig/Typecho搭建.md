
### 官方文档以及教程

* [官方安装教程](https://www.typechodev.com/docs/zh_CN/user-guide/install/)
* [参考安装教程](https://black1ce.com/website/centos7-install-typecho.html)
* [参考安装教程](https://blog.csdn.net/u010694718/article/details/86233620)

### 安装web服务器

* yum -y install httpd           # 安装主程序
* systemctl start httpd.service  # 启动服务
* systemctl status httpd.service # 查看服务运行状态

#### nginx 端口和 httpd冲突

```
Job for httpd.service failed because the control process exited with error code. See "systemctl 
```

* 修改httpd端口即可 `vim /etc/httpd/conf/httpd.conf`
* 找到Listen ,修改端口重新启动

* 查看状态显示运行即为启动成功

### 安装php和相关插件
Typecho官方要求PHP5.1，但是据说PHP7的性能是PHP5的两倍，所以我们直接安装PHP7好了。由于Centos内置源的版本比较老，所以我们要先更换一下PHP的源，输入如下指令：
```
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm   
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
```
* yum -y remove php*    #卸载旧版本PHP
* yum -y install php    #安装PHP
* yum -y install php-mysql php-gd php-ldap php-odbc php-pear php-xml php-xmlrpc php-mbstring php-snmp php-soap curl curl-devel php-sqlite3   #安装常用插件

>如果安装过旧版的php存在冲突可以加上版本 ，例如`yum -y install php70w-gd`
### 官网获取Typecho主程序
* 访问[官方下载地址](http://typecho.org/download)获取稳定版主程序压缩包。解压后将build文件夹内所有文件全部上传到服务器的/var/www/html目录下，不包括build文件夹本身。

* 主程序上传完后执行如下指令提升权限，这一步很重要，不然Typecho没有办法完成安装
    * chmod -R 777 /var/www/html    # 提升权限

* 输入如下指令重启Apache服务：
  
  * systemctl restart httpd.service    #重启Apache服务
  
* 访问域名完成配置即可  


### 数据库创建
* CREATE DATABASE typecho DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;



### 升级到php7

```
默认的版本太低了，手动安装有一些麻烦，想采用Yum安装的可以使用下面的方案：
首先删除旧版本的PHP，
通过yum list installed | grep php可以查看所有已安装的php软件
使用yum remove php*删除

通过yum list php*查看是否有自己需要安装的版本，如果没有就需要添加第三方yum源， 推荐安装webtatic、rpmforge，还有国内163的

CentOs 5.x 
rpm -Uvh http://mirror.webtatic.com/yum/el5/latest.rpm 
CentOs 6.x 
rpm -Uvh http://mirror.webtatic.com/yum/el6/latest.rpm 
CentOs 7.X 
rpm -Uvh https://mirror.webtatic.com/yum/el7/epel-release.rpm 
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
#32位：  
wget http://packages.sw.be/rpmforge-release/rpmforge-release-0.5.1-1.el5.rf.i386.rpm  
rpm -ivh rpmforge-release-0.5.1-1.el5.rf.i386.rpm  
#64位：  
wget http://packages.sw.be/rpmforge-release/rpmforge-release-0.5.1-1.el5.rf.x86_64.rpm  
rpm -ivh rpmforge-release-0.5.1-1.el5.rf.x86_64.rpm  
安装完成后可以使用yum repolist查看已经安装的源，也可以通过ls /etc/yum.repos.d/查看。 

然后再yum install php55w或yum install php54w或yum install php56w||yum install php70w……就可以安装新版本PHP了 




```




* 查看当前php版本

`$ php -v`


* 检查当前PHP的安装包

`$ yum list installed | grep php`


* 移除当前PHP的安装包

`$ yum remove php*`


* 添加第三方的YUM源

```CentOS 6.5：
$ rpm -Uvh http://mirror.webtatic.com/yum/el6/latest.rpm

CentOS 7.x：
$ rpm -Uvh https://mirror.webtatic.com/yum/el7/epel-release.rpm
$ rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
```

* 查看YUM源上能用PHP安装包

`$ yum list php*`


* 安装php7

`$ yum install php70w`


### 相关命令
```

apache
启动
systemctl start httpd
停止
systemctl stop httpd
重启
systemctl restart httpd
或者

service httpd stop

service httpd start

service httpd restart


mysql
启动
systemctl start mysqld
停止
systemctl stop mysqld
重启
systemctl restart mysqld

或者

service mysqld stop

service mysqld start

service mysqld restart



php-fpm
启动
systemctl start php-fpm
停止
systemctl stop php-fpm
重启
systemctl restart php-fpm


nginx
启动
systemctl start nginx
停止
systemctl stop nginx
重启
systemctl restart nginx

或者

service nginx stop
service nginx start
service nginx restart
开机自启

chkconfig httpd on

chkconfig mysqld on


```



### 删除nginx日志的时候粗心大意删除`nginx.pid`导致NGINX报错
```
"/usr/local/nginx/logs/nginx.pid" failed (2: No such file or directory)

```


* nginx停止`/usr/local/nginx/sbin/nginx -s stop`
* NGINX重启 `/usr/local/nginx/sbin/nginx -s reload`

* 解决办法 `/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf`

    * 原理：使用nginx -c的参数指定nginx.conf文件的位置

>但是nginx.pid被删除会出现如下问题

* 进程端口被占用无法完成配置，需要先杀掉NGINX的进程
* lsof -i:80

```
[root@izwz993bv9azta8nrfwuhqz sbin]# /usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] still could not bind()
[root@izwz993bv9azta8nrfwuhqz sbin]# /usr/local/nginx/s
sbin/      scgi_temp/
[root@izwz993bv9azta8nrfwuhqz sbin]# /usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (98: Address already in use)
nginx: [emerg] still could not bind()
[root@izwz993bv9azta8nrfwuhqz sbin]# kill -9 80
-bash: kill: (80) - 没有那个进程
[root@izwz993bv9azta8nrfwuhqz sbin]# lsof -i :80
COMMAND     PID   USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
AliYunDun  2175   root   20u  IPv4  18156      0t0  TCP izwz993bv9azta8nrfwuhqz:39954->100.100.30.25:http (ESTABLISHED)
nginx     13994   root    6u  IPv4 203889      0t0  TCP *:http (LISTEN)
nginx     31604 nobody    6u  IPv4 203889      0t0  TCP *:http (LISTEN)
[root@izwz993bv9azta8nrfwuhqz sbin]# kill -9 13994
```


### 打开页面出现502的问题
* 其实是理解错误,nginx的代理配置需要指向php的端口而不是httpd的端口,关键是端口不能被占用;另外需要注意nagix的日志信息
* 其实感觉httpd也可以但是这里不使用这种方式

* nginx配置如下
* 注意启动方式,端口不能被占用,通过nginx访问
```
启动php-cgi：
    1. php-cgi -b 127.0.0.1:8000 -c /etc/php.ini;
    # 注意这只是前台启动
    2. 开机自启动：
        cat /etc/init.d/S51phpCGI
        echo "Start php-cgi."
        php-cgi -b 127.0.0.1:8000 -c /etc/php.ini &
```

```
server {
    listen          80;
    server_name      www.imisty.cn imisty.cn;
    root            /var/www/html/typecho;
    index           index.html index.htm index.php;

    if (!-e $request_filename) {
        rewrite ^(.*)$ /index.php$1 last;
    }

    location ~ .*\.php(\/.*)*$ {
        include fastcgi.conf;
        fastcgi_pass  127.0.0.1:8000;
    }

    access_log logs/misty.log combined;
}

```

### php-cgi启动一会就会无缘无故关闭
* 网传关闭Apache的httpdservice就行了
* 另外一种,推荐 使用php-fpm 管理php-cgi

### 页面显示数据库错误
![](/images/image22.png)
* 网传升级到php5.6之后就没这个问题了,但是出现了数据库适配器无法选择的问题


```
PHP Startup: Unable to load dynamic library '/usr/lib64/php/modules/msql.so' - /usr/lib64/php/modules/msql.so: cannot open shared object file: No such file or directory in Unknown on line 0
```


### 打开php.ini打开mysql扩展
* 开启 extension_dir = mysql...,将前面的`;`删除即可 （注意不同系统的开启方式不一致,windows下为***.dll,unux下为***.so）

### 启动php-cgi无法加载mysql.so,文件不存在
* yum -y install php-mysql 安装数据库扩展即可

>这里安装了数据库扩展，php-cgi启动也会出现mysql.so的问题，但是不影响页面数据库适配器加载了，也不影响启动和安装

 
### 因为之前安装过旧版的php,安装mysql扩展出现依赖冲突

* 先移除依赖冲突的相关包
```

 yum remove php-common
已加载插件：fastestmirror
正在解决依赖关系
--> 正在检查事务
---> 软件包 php56w-common.x86_64.0.5.6.40-1.w7 将被 删除
--> 正在处理依赖关系 php56w-common(x86-64) = 5.6.40-1.w7，它被软件包 php56w-5.6.40-1.w7.x86_64 需要
--> 正在处理依赖关系 php56w-common(x86-64) = 5.6.40-1.w7，它被软件包 php56w-gd-5.6.40-1.w7.x86_64 需要
--> 正在处理依赖关系 php56w-common(x86-64) = 5.6.40-1.w7，它被软件包 php56w-cli-5.6.40-1.w7.x86_64 需要
--> 正在处理依赖关系 php56w-common(x86-64) = 5.6.40-1.w7，它被软件包 php56w-pdo-5.6.40-1.w7.x86_64 需要
--> 正在检查事务
---> 软件包 php56w.x86_64.0.5.6.40-1.w7 将被 删除
---> 软件包 php56w-cli.x86_64.0.5.6.40-1.w7 将被 删除
---> 软件包 php56w-gd.x86_64.0.5.6.40-1.w7 将被 删除
---> 软件包 php56w-pdo.x86_64.0.5.6.40-1.w7 将被 删除
--> 解决依赖关系完成

依赖关系解决

```
* 再安装php相关扩展
```
yum -y install php-mysql

已加载插件：fastestmirror
Loading mirror speeds from cached hostfile
 * webtatic: us-east.repo.webtatic.com
正在解决依赖关系
--> 正在检查事务
---> 软件包 php-mysql.x86_64.0.5.4.16-46.el7 将被 安装
--> 正在处理依赖关系 php-pdo(x86-64) = 5.4.16-46.el7，它被软件包 php-mysql-5.4.16-46.el7.x86_64 需要
--> 正在检查事务
---> 软件包 php-pdo.x86_64.0.5.4.16-46.el7 将被 安装
--> 正在处理依赖关系 php-common(x86-64) = 5.4.16-46.el7，它被软件包 php-pdo-5.4.16-46.el7.x86_64 需要
--> 正在检查事务
---> 软件包 php-common.x86_64.0.5.4.16-46.el7 将被 安装
--> 解决依赖关系完成
```

>可惜到这里还是出现了如下错误,因为之前安装了旧版的php，没有卸载干净
```
---> 软件包 php-common.x86_64.0.5.4.16-46.el7 将被 安装
--> 处理 php70w-common-7.0.33-1.w7.x86_64 与 php-common < 7.0 的冲突
--> 解决依赖关系完成
错误：php70w-common conflicts with php-common-5.4.16-46.el7.x86_64
 您可以尝试添加 --skip-broken 选项来解决该问题
 您可以尝试执行：rpm -Va --nofiles --nodigest

```
* 思路：卸载php-common,结果将php卸载了,`php -v`命令都识别不了;既然5.6版本也无法识别数据库适配器，索性全部更换了php7,具体安装详见上

* 最终解决办法,在相关的扩展后面加上版本就好` yum -y install php70w-mysql`

>注意：之前因为下载不了包,修改了yum.repo.d文件的,将当前的数据源协议改为 http,这里还是改回https,网上的说法不要乱套用，还是要自己思考一下的


* 至此,重启php服务,刷新一下界面,typecho就出现了数据库扩展,官方说使用mysql原生的，但是我这里没有看到，选择了另外一个，不影响安装
```
[root@izwz993bv9azta8nrfwuhqz yum.repos.d]# kill -9 5470
[root@izwz993bv9azta8nrfwuhqz yum.repos.d]# php-cgi -b 127.0.0.1:8000 -c /etc/php.ini &

```
>后来使用wordpress的过程中,发现可以不启动,直接用nginx管理静态网站即可,可惜这个时候已经没有使用typecho了


* 放一张成功的图片，大吉大利今晚吃鸡,来来去去折腾了三天，终于搞定了,还好没有放弃
![](/images/image23.png)

