* MySql下载路径 `https://dev.mysql.com/downloads/installer/ `
* 建议使用安装包形式（离线安装）而不要使用安装器
* 安装使用系统的默认路径好一点，然后注意配置好环境变量;
* 注意赋予相关目录管理员权限;有点稀里糊涂，但是一旦安装失败要点击先remove相关组件然后再添加，而不要全部直接remove，另外移除之后需要重启系统

