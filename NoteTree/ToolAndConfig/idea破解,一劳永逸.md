IDEA破解补丁破解
* http://idea.lanyus.com/上可以找到最新的破解补丁，下载并放到软件的bin目录下 
* 更改bin目录下的两个文件：Idea.exe.vmoptions和Idea64.exe.vmoptions，在文件末尾加上如： 
* `-javaagent:C:\IntelliJ IDEA 2017.3.5\bin\JetbrainsCrack-2.7-release-str.jar` 的补丁地址
![](/images/image1.png)
![](/images/image2.png)
* 激活方式选择Activation code,然后输入如下代码，即可完成破解
```
ThisCrackLicenseId-{ 
“licenseId”:”ThisCrackLicenseId”, 
“licenseeName”:”iMisty”, 
“assigneeName”:”iMisty@qq.com“, 
“assigneeEmail”:”随便填一个邮箱(我填的:idea@163.com)”, 
“licenseRestriction”:”For This Crack, Only Test! Please support genuine!!!”, 
“checkConcurrentUse”:false, 
“products”:[ 
{“code”:”II”,”paidUpTo”:”2099-12-31”}, 
{“code”:”DM”,”paidUpTo”:”2099-12-31”}, 
{“code”:”AC”,”paidUpTo”:”2099-12-31”}, 
{“code”:”RS0”,”paidUpTo”:”2099-12-31”}, 
{“code”:”WS”,”paidUpTo”:”2099-12-31”}, 
{“code”:”DPN”,”paidUpTo”:”2099-12-31”}, 
{“code”:”RC”,”paidUpTo”:”2099-12-31”}, 
{“code”:”PS”,”paidUpTo”:”2099-12-31”}, 
{“code”:”DC”,”paidUpTo”:”2099-12-31”}, 
{“code”:”RM”,”paidUpTo”:”2099-12-31”}, 
{“code”:”CL”,”paidUpTo”:”2099-12-31”}, 
{“code”:”PC”,”paidUpTo”:”2099-12-31”} 
], 
“hash”:”2911276/0”, 
“gracePeriodDays”:7, 
“autoProlongated”:false}
```