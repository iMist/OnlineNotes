http://bbs.gfan.com/android-8340286-1-1.html


```



 利用 mount 来将某个目录挂载到另外一个目录去！这并不是挂载文件系统，而是挂载某个目录！虽然也可以使用 ln -s 命令建立软链接，但在某些程序不支持符号链接，所以就是得要通过这样的方法来达到类似“链接”到某目录下的作用。
 
 原本想用/system/bin/目录用ln -s的方式挂载到/sdcard0/下，但是发现system分区是ext4文件系统（基于内核的文件系统），而sdcard是fuse文件系统（基于用户空间的文件系统），Android系统下ext4和fuse不同的文件系统不能用ln -s软链接（Linux可以跨文件系统ln -s软链接）.Google之后，发现mount -o bind 这种方式可以在不同的文件系统挂载。备忘记录下!
 
`-B, --bind`
将某个目录树绑定挂载到其它地方，这样就可以同时从两个地方进行访问。
# mount --bind /system/test /sdcard/111
 
`-R, --rbind`
将某个目录树绑定挂载到其它地方，并且其子目录如果是挂载点的话也递归的进行绑定。
# mount --rbind /system/test /sdcard/111
 
 
1、挂载目录：把/system/test/ 挂载到/storage/sdcad0/111目录下
# mkdir test
# cd test
# touch text.txt
# mount -o bind /system/test/ /storage/sdcad0/111
    
或# busybox mount --bind  /system/test/ /storage/sdcad0/111
 
2、取消挂载
# umount /storage/sdcad0/111
 
3、开机直接挂载
修改/etc/fstab文件
 
# <file system> <mount point> <type> <options> <dump> <pass>  
  /system/test/ /storage/sdcad0/111 none  rw,bind 0 0  

原文：https://blog.csdn.net/u010164190/article/details/54945551 
```

> 注意挂载/取消挂载最好使用绝对路径

### 挂载目录分析
* 刚开始尝试的时候因为进行数据迁移,没有能够使用,但是数据迁移之后,默认使用SD卡的话,使用了应用的SD卡上的数据,重启手机点击app出现应用未安装,图标消失
* 和手机自带的内部的存储一样，扩展的SD卡内部存储一部分挂载在外部空间`/storage/emulated       14.6G   369.0M    14.2G   4096`一部分挂载在内部空间`/mnt/expand/b9171682-f8d9-4103-8a99-717f0d92bb95`


```
root@www.aialbb.com:/data/data # df
Filesystem               Size     Used     Free   Blksize
/dev                   935.2M   116.0K   935.1M   4096
/sys/fs/cgroup         935.2M    12.0K   935.2M   4096
/mnt                   935.2M     0.0K   935.2M   4096
/system                  2.4G     1.3G     1.1G   4096
/data                   11.2G     3.4G     7.8G   4096
/cache                 418.4M   392.0K   418.1M   4096
/protect_f               3.9M    68.0K     3.8M   4096
/protect_s               8.0M    60.0K     8.0M   4096
/nvdata                 27.5M     5.7M    21.8M   4096
/storage               935.2M     0.0K   935.2M   4096
/mnt/expand/b9171682-f8d9-4103-8a99-717f0d92bb95    14.6G   369.0M    14.2G   4096
/storage/emulated       14.6G   369.0M    14.2G   4096
/mnt/runtime/default/emulated    14.6G   369.0M    14.2G   4096
/mnt/runtime/read/emulated    14.6G   369.0M    14.2G   4096
/mnt/runtime/write/emulated    14.6G   369.0M    14.2G   4096

```


