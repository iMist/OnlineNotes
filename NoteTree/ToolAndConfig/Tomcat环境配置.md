
* 在启动tomcat之前必须要配置好java环境
* 将tomcat的安装目录配置进入环境变量
* 但是JAVA环境必须要配置JAVA_HOME,也就是jdk的安装路径，启动需要识别这参数，不然tomcat无法启动,环境配置以下图为标准；
![](/images/image4.png)
* 然后在tomcat的安装目录的bin目录下点击startup.bat（windows环境下）
![](/images/image5.png)
* 浏览器输入 `localhost:8080` 出现tomcat的默认首页，说明环境变量配置成功;