### Windows java环境变量配置
* 下载java https://www.oracle.com/technetwork/java/javaee/downloads/index.html

* 安装java，注意jdk路径和jre路径

* 打开系统环境变量部分；,如果用JAVA_HOME变量需要注意JAVAHOME变量的引用方式 %JAVA_HOME% 
![](/images/image3.png)

* 配置jre 环境变量 
![](/images/image6.png)