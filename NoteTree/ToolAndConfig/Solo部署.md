![](https://img.hacpai.com/bing/20171222.jpg?imageView2/1/w/960/h/540/interlace/1/q/100)

##### 官方文档
[官方部署文档](https://github.com/b3log/solo)

### 推荐使用Docker部署
* docker 环境部署（略）

#### 注意官网不推荐使用源码构建
>我们不建议通过 war 发布包或者源码构建部署，因为这样的部署方式在将来有新版本发布时升级会比较麻烦。 这两种方式请仅用于本地试用，线上生产环境建议通过 Docker 部署。


###  docker部署

* `docker pull b3log/solo`

 ```
docker run --detach --name solo --network=host     --env RUNTIME_DB="MYSQL"     --env JDBC_USERNAME="misty"  --env JDBC_PASSWORD=123456!  --env JDBC_DRIVER="com.mysql.cj.jdbc.Driver"     --env JDBC_URL="jdbc:mysql://127.0.0.1:3306/solo?useUnicode=yes&allowPublicKeyRetrieval=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC"     b3log/solo --listen_port=8085 --server_scheme=http --server_host=imisty.cn
```

* docker start solo 

* 查看日志 `docker logs solo`

### 实际部署却并不容易

#### 脚本运行特殊字符导致命令截断问题
```
//无论单引号还是双引号都会出现截断的问题
docker run --detach --name solo --network=host     --env RUNTIME_DB="MYSQL"     --env JDBC_USERNAME="misty"  --env JDBC_PASSWORD="123456\!"   --env JDBC_DRIVER="com.mysql.cj.jdbc.Driver"     --env JDBC_URL="jdbc:mysql://127.0.0.1:3306/solo?useUnicode=yes&allowPublicKeyRetrieval=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC"     b3log/solo --listen_port=8000 --server_scheme=http --server_host=imisty.cn

docker run --detach --name solo --network=host     --env RUNTIME_DB="MYSQL"     --env JDBC_USERNAME="misty"  --env JDBC_PASSWORD='123456\!'  --env JDBC_DRIVER="com.mysql.cj.jdbc.Driver"     --env JDBC_URL="jdbc:mysql://127.0.0.1:3306/solo?useUnicode=yes&allowPublicKeyRetrieval=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC"     b3log/solo --listen_port=8000 --server_scheme=http --server_host=imisty.cn

```

* 最后解决方式参考先前的部署步骤

 [参考博文](http://www.cnblogs.com/chuanzhang053/p/9253410.html)

#### Mysql8新特性要求
```
Public Key Retrieval is not allowed
	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:110)
	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:97)
	at com.mysql.cj.jdbc.exceptions.SQLExceptionsMapping.translateException(SQLExceptionsMapping.java:122)
	at com.mysql.cj.jdbc.ConnectionImpl.createNewIO(ConnectionImpl.java:832)
	at com.mysql.cj.jdbc.ConnectionImpl.<init>(ConnectionImpl.java:456)
	at com.mysql.cj.jdbc.ConnectionImpl.getInstance(ConnectionImpl.java:240)
	at com.mysql.cj.jdbc.NonRegisteringDriver.connect(NonRegisteringDriver.java:207)
```
* 连接url加上 `allowPublicKeyRetrieval=true`即可

##### 实际部署过程中还出现了8000 端口无法使用的问题

* 为了方便记忆和管理，占用8000 端口出现了内部错误，不深究了，换端口即可

