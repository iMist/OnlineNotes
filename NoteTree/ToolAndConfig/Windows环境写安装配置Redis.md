
### Windows 环境下的redis环境配置
[参考地址](https://www.jianshu.com/p/e16d23e358c0)

### 下载 
* [官网地址](https://redis.io/download)
* [微软的windows版本地址](https://github.com/MicrosoftArchive/redis/tags)

* 下载的时候下载msi(不要下载zip的压缩包)

#### 安装
* 点击下载好的文件安装 `  下一步` 即可

* 设置Redis的服务端口   默认为6379   
![](https://upload-images.jianshu.io/upload_images/4630295-0f873f3227c5ad47.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/507/format/webp)

* 选择安装的路径，并且打上勾（这个非常重要），添加到path是把Redis设置成windows下的服务，不然你每次都要在该目录下启动命令redis-server redis.windows.conf，但是只要一关闭cmd窗口，redis就会消失，这样就比较麻烦

![](https://upload-images.jianshu.io/upload_images/4630295-045ab227dece2445.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/499/format/webp)

* 如果redis的应用场景是作为db使用，那不要设置这个选项，因为db是不能容忍丢失数据的。如果作为cache缓存那就得看自己的需要（我这里设置了1024M的最大内存限制）指定Redis最大内存限制，Redis在启动时会把数据加载到内存中，达到最大内存后，Redis会先尝试清除已到期或即将到期的Key，当此方法处理 后，仍然到达最大内存设置，将无法再进行写入操作，但仍然可以进行读取操作。Redis新的vm机制，会把Key存放内存，Value会存放在swap区。
  

### 测试安装

* 通过msi文件的安装，可以在计算机管理→服务与应用程序→服务  看到Redis正在运行，这里先关闭，不然有命令行启动会出现端口占用的情况

* cmd窗口进入Redis的安装路径的根目录 输入命令 `redis-server.exe redis.windows.conf`, 启动redis

* 你可以在Redis的安装根目录下找到redis-cli.exe文件启动客户端, 或在cmd中先进入Redis的安装根目录用命令redis-cli.exe -h [localhost|ip] -p 6379（注意换成自己的IP）的方式打开


 
