### 相关文档
* [根据这个成功安装](https://testerhome.com/topics/10406)

* [MAC下的环境搭建：](http://blog.csdn.net/tobetheender/article/details/52905730)
    * 需要补充一个 brew install yasm
* [Linux下的环境搭建：](https://testerhome.com/topics/2988)

* [STF手机设备管理平台连接其它操作系统上的安卓设备实操介绍](http://www.cnblogs.com/jinjiangongzuoshi/p/6555696.html)

### ubuntu环境下Docker安装 STF

* 更新软件 sudo apt-get update
* 安装 sudo apt-get install docker
* 安装 sudo apt-get install docker.io
* 查看 docker 镜像 sudo docker images

#### 拉取STF镜像 
* sudo docker pull openstf/stf:latest # STF镜像
* sudo docker pull sorccu/adb:latest # android adb 镜像
* sudo docker pull rethinkdb:latest # rethinkdb 镜像
* sudo docker pull openstf/ambassador:latest
* sudo docker pull nginx:latest # nginx 代理镜像
* 查看已经拉去好的镜像 sudo docker images

![](/images/docker_stf.png)

* 如果是虚拟机需要改变虚拟网卡为桥接模式

* 查看本机的IP地址 `ifconfig`,如果发现没有该命令执行 `apt-get install net-tools`

* 虚拟机的本机ip192.168.1.100 这个IP地址是根据你所在的网络自动分配的
    * 如 enp0s3 192.168.1.100 在物理机上 ping 192.168.1.100 是否能ping通
    
#### 启动镜像

* 先启动一个数据库
`docker run -d --name rethinkdb -v /srv/rethinkdb:/data --net host rethinkdb rethinkdb --bind all --cache-size 8192 --http-port 8090`

* 再启动adb service
`docker run -d --name adbd --privileged -v /dev/bus/usb:/dev/bus/usb --net host sorccu/adb:latest`

* 再启动stf 启动的时配置的IP地址为你虚拟机桥接的网址 enp0s3 
`docker run -d --name stf --net host openstf/stf stf local --public-ip 192.168.1.100  `

* 查看 启动的docker镜像 命令： `sudo docker ps -a    `

* 浏览器访问 `192.168.1.100:7100`


### Docker 相关命令
* docker pull images_name:version 从公网拉取一个镜像

*  docker images 查看已有的docker镜像

* sudo docker ps -a 查看当前容器的运行状态 

* docker start 容器id/容器名称 启动一个容器

* sudo docker stats 查看容器运行占用的系统资源

* `sudo docker container rm [容器id]/容器名` 删除容器，如果要删除的容器是运行状态需要停止

* `sudo docker container stop [容器id]/容器名`停止在运行的容器

* docker image rm [镜像id] 如果遇到不能删除的问题，可能是容器还在使用这个镜像，需要将不使用的容器完全删除才能删除镜像

* docker logs -f  CONTAINER_ID 查看容器运行的日志

#### 安装运行过程中遇到的问题
* 启动docker容器adbd，但是启动立刻exited ,查看docker日志 `error: could not install *smartsocket* listener: cannot bind to 127.0.0.1:5037:`
    * 5037 端口被占用，找到占用的端口关闭；
    * 或者直接adb kill-server （启动 adb start-server） 释放服务资源
    * 然后`sudo docker restart adbd` 使用 docker ps -a 查看容器运行状态，运行中即可
    
* 虚拟机上需要将设备连接在虚拟机上而不能连接在物理机上    


### 其他手机投屏远程管理方案

* [scrcpy](https://github.com/Genymobile/scrcpy)
    
* [atx-server](https://github.com/openatx/atx-server)
   * [安装方法](https://testerhome.com/topics/11588)
    


