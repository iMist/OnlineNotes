
### 参考博文
* [参考掘金系列博文](https://juejin.im/post/5be8dff2f265da61417118ed)
* [参考博文](https://github.com/openthos/xposed-analysis/blob/master/docs/%E5%88%86%E6%9E%90%E3%80%81%E7%BC%96%E8%AF%91%E3%80%81%E5%AE%89%E8%A3%85%E3%80%81%E8%BF%90%E8%A1%8CXposed.md)

* [源码编译和镜像编译](https://blog.csdn.net/u013115811/article/details/79858959)

* [参考博文](https://blog.csdn.net/fuchaosz/article/details/52473660)

###编译Xposed
* 参考博文 https://www.jianshu.com/p/c731cd44e82b
* 参考博文 https://www.cnblogs.com/luoyesiqiu/p/9524651.html
#### Xposed简介
* Xposed是Android平台上的有名的Hook工具，用它可以修改函数参数，函数返回值和类字段值等等，也可以用它来进行调试。Xposed有几个部分组成：修改过的android_art,这个项目修改部分art代码，使Hook成为可能
* Xposed native部分，该部分主要提供给XposedBridge可调用api和调用修改过的android_art的api,还有生成可替换的app_process程序
* XposedBridge,该项目是主要功能是提供给Xposed的模块开发者api，它将编译成XposedBridge.jar
* XposedInstaller,该项目是Xposed安装器，使得普通用户在使用Xposed更方便，同时，它还可以管理手机上已经安装的Xposed模块，它编译完成后将生成apk文件，本文不讨论如何编译它。

###编译准备
* Ubuntu系统，推荐16.04及以上，本文用的18.04
* Android Studio
* Android源码(下载链接，请百度)
* 修改过的android_art:https://github.com/rovo89/android_art
* Xposed native部分：https://github.com/rovo89/Xposed
* XposedBridge:https://github.com/rovo89/XposedBridge
* Xposed构建工具，XposedTools:https://github.com/rovo89/XposedTools

### 配置
* Android-ART
    * 将Android源码下的art目录移动到其他路径备份，比如Android源码的上层路径
    * 在Android源码路径执行`git clone https://github.com/rovo89/android_art -b xposed-nougat-mr2 art`,注意根据Android源码版本选择分支或者对应的标签
* Xposed Native
    * 转到frameworks/base/cmds目录,执行git clone https://github.com/rovo89/Xposed xposed，将Xposed Native部分的源码下载    

* XposedBridge
    * 在任意目录执行git clone https://github.com/rovo89/XposedBridge -b art，然后导入Android Studio中，点Build->Rebuild Project，会在app/build/intermediates/transform/preDex/release目录下生成.jar文件，将生成的jar文件重命名为XposedBridge.jar，放入Android源码目录下的out/java/下。也可以直接生成apk,然后将生成的apk后缀改为jar
    * 将生成的jar导入`aosp/out/target/product/hammerhead/system/framework/`
    
>这里jar文件存放的目录不同的博文是不一致的，但是觉得framework里面靠谱一点    

>注：如果想生成供Xposed模块调用的XposedBridge.jar，则在Android Studio的右侧打开Gradle Project，双击jarStubs就会在app/build/api生成api.jar

* XposedTools
    * 在任意目录执行git clone https://github.com/rovo89/XposedTools,将XposedTools目录下的build.conf.sample复制一份，并将它重命名为build.conf，build.conf文件用于配置构建环境，我们来看他的内容：
    
```
[General]
outdir = /home/misty/bin/WORKING_DIRECTORY/out
#javadir = /android/XposedBridge

[Build]
# Please keep the base version number and add your custom suffix
version = 86 (custom build by xyz / %s)
# makeflags = -j4

[GPG]
sign = release
user = 852109AA!

# Root directories of the AOSP source tree per SDK version
[AospDir]
#19 = /android/aosp/440
#21 = /android/aosp/500
23 = /home/misty/bin/WORKING_DIRECTORY
                                                             
# SDKs to be used for compiling BusyBox
# Needs https://github.com/rovo89/android_external_busybox

#[BusyBox]
#arm = 23
#x86 = 23
#armv5 = 23



* outdir:指定Android源码中的out目录
* javadir:指定XposedBridge目录，如果你不需要编译XposedBridge.jar可以不指定
* version:Xposed版本，这个版本号将显示在XposedInstaller上
* ApospDir下的数字:设置sdk版本对应的Android源码
* [BusyBox]标签:busybox，可以不指定
```    

* 配置完成后，就可以执行build.pl编译了,以下有几个例子：

`./build.pl -a java`

* 编译XposedBridge.jar，需要在build.conf里指定javadir

`./build.pl -t arm64:23`

* 编译生成供cpu架构为arm64，sdk为23平台使用的Xposed

* 编译完成后，将在Android源码目录/out/sdk23/arm生成可刷入手机的zip文件


### 配置 perl 环境


* XposedTools 依赖于 perl，所以我们要跑起来就要有一个 perl 环境。首次运行 perl 需要安装依赖 Config::IniFiles
`perl -MCPAN -e 'install Config::IniFiles'`

* 大概需要在安装下面的两个依赖（不同系统可能不同）
`perl -MCPAN -e 'install File::Tail'`
`perl -MCPAN -e 'install File::ReadBackwards'`

* 如果还不成功，按照下面的方法安装所需的模块即可
`perl -MCPAN -e 'install [ModuleName]'`

#### 解决模块安装失败的问题
* 执行build.pl的时候提示找不到函数，比如提示找不到Config::IniFiles. 可以通过下面的方式来寻找并安装依赖：
    * 执行apt-cache search Config::IniFiles寻找Config::IniFiles所依赖的库文件
    * 执行sudo apt install libconfig-inifiles-perl安装所依赖的库
```
    libconfig-inifiles-perl - Read .ini-style configuration files
```
   
    
* 使用aptitude工具下载依赖 
    * sudo aptitude install Config::IniFiles
    * sudo aptitude install File::Tail
    * sudo aptitude install File::ReadBackwards
>虽然可能会出现安装失败,但是会出现依赖提醒,按照提示安装依赖即可   


### 常见问题
* art更名替换导致如下错误,不可以在源码根目录下,需要移动到其他目录，本例移动到上级目录
```
build/core/base_rules.mk:157: *** art_bak: MODULE.TARGET.FAKE.cpplint-art-phony already defined by art。 停止。
   
   #### make failed to build some targets (6 seconds) ####

```



### 相关命令
* `getprop|grep arm ` 查看CPU相关的信息
